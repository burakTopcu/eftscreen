//
//  GonderenHesapBilgisi.swift
//  EFT
//
//  Created by Burak Topçu on 11.03.2018.
//  Copyright © 2018 Burak Topçu. All rights reserved.
//

import Foundation

public struct Item {
//    var name: String
//    var detail: String

    var hesapNo : Int
    var semt : String
    var bakiye : Double
    var kullanilabilirBakiye : Double
    
//    public init(name: String, detail: String) {
//        self.name = name
//        self.detail = detail
//    }
    public init(hesapNo: Int, semt: String, bakiye: Double, kullanilabilirBakiye: Double){
        self.hesapNo = hesapNo
        self.semt = semt
        self.bakiye = bakiye
        self.kullanilabilirBakiye = kullanilabilirBakiye
        
    }
}

public struct Section {
    var name: String
    var items: [Item]
    var collapsed: Bool
    
    public init(name: String, items: [Item], collapsed: Bool = true) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}

public var sectionsData: [Section] = [
    Section(name: "Hesaplar", items: [
        Item(hesapNo: 1234567, semt: "Mecidiyeköy", bakiye: 500.57, kullanilabilirBakiye: 750.88),
        Item(hesapNo: 9415151, semt: "Levent", bakiye: 980.02, kullanilabilirBakiye: 1980.09),
        Item(hesapNo: 1236427, semt: "Beşiktaş", bakiye: 560.53, kullanilabilirBakiye: 780.84),
        Item(hesapNo: 1234567, semt: "Kadıköy", bakiye: 534.50, kullanilabilirBakiye: 723.82),
        Item(hesapNo: 1234567, semt: "Şişli", bakiye: 506.50, kullanilabilirBakiye: 787.80)
        ])]
