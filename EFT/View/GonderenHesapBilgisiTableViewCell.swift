//
//  GonderenHesapBilgisiTableViewCell.swift
//  EFT
//
//  Created by Burak Topçu on 11.03.2018.
//  Copyright © 2018 Burak Topçu. All rights reserved.
//

import UIKit

class GonderenHesapBilgisiTableViewCell: UITableViewCell {

    let hesapNoSemtLabel = UILabel()
    let bakiyeLabel = UILabel()
    let kullanilabilirBakiyeLabel = UILabel()
    let bakiyeBirimLabel = UILabel()
    let kullanilabilirBakiyeBirimLabel = UILabel()
    
    // MARK: Initalizers
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let marginGuide = contentView.layoutMarginsGuide
        
        contentView.addSubview(hesapNoSemtLabel)
        hesapNoSemtLabel.textColor = UIColor.black
        hesapNoSemtLabel.font = UIFont.boldSystemFont(ofSize: 15)
        hesapNoSemtLabel.sizeToFit()
        hesapNoSemtLabel.translatesAutoresizingMaskIntoConstraints = false
        hesapNoSemtLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        hesapNoSemtLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        
        contentView.addSubview(kullanilabilirBakiyeLabel)
        kullanilabilirBakiyeLabel.textColor = UIColor.black
        kullanilabilirBakiyeLabel.font = bakiyeLabel.font.withSize(15)
        kullanilabilirBakiyeLabel.sizeToFit()
        kullanilabilirBakiyeLabel.translatesAutoresizingMaskIntoConstraints = false
        kullanilabilirBakiyeLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        kullanilabilirBakiyeLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        
        
        contentView.addSubview(bakiyeLabel)
        bakiyeLabel.textColor = UIColor.black
        bakiyeLabel.font = bakiyeLabel.font.withSize(15)
        bakiyeLabel.sizeToFit()
        bakiyeLabel.translatesAutoresizingMaskIntoConstraints = false
        bakiyeLabel.bottomAnchor.constraint(equalTo: kullanilabilirBakiyeLabel.topAnchor, constant: -5.0).isActive = true
        bakiyeLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        
        contentView.addSubview(bakiyeBirimLabel)
        bakiyeBirimLabel.textColor = UIColor.black
        bakiyeBirimLabel.font = UIFont.boldSystemFont(ofSize: 15)
        bakiyeBirimLabel.sizeToFit()
        bakiyeBirimLabel.translatesAutoresizingMaskIntoConstraints = false
        bakiyeBirimLabel.topAnchor.constraint(equalTo: bakiyeLabel.topAnchor).isActive = true
        bakiyeBirimLabel.bottomAnchor.constraint(equalTo: bakiyeLabel.bottomAnchor).isActive = true
        bakiyeBirimLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: -5.0).isActive = true
        
        contentView.addSubview(kullanilabilirBakiyeBirimLabel)
        kullanilabilirBakiyeBirimLabel.textColor = UIColor.black
        kullanilabilirBakiyeBirimLabel.font = UIFont.boldSystemFont(ofSize: 15)
        kullanilabilirBakiyeBirimLabel.sizeToFit()
        kullanilabilirBakiyeBirimLabel.translatesAutoresizingMaskIntoConstraints = false
        kullanilabilirBakiyeBirimLabel.topAnchor.constraint(equalTo: kullanilabilirBakiyeLabel.topAnchor).isActive = true
        kullanilabilirBakiyeBirimLabel.bottomAnchor.constraint(equalTo: kullanilabilirBakiyeLabel.bottomAnchor).isActive = true
        kullanilabilirBakiyeBirimLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: -5.0).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
