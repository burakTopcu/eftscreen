//
//  Extensions.swift
//  EFT
//
//  Created by Burak Topçu on 11.03.2018.
//  Copyright © 2018 Burak Topçu. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        
        self.layer.add(animation, forKey: nil)
    }
    
}

