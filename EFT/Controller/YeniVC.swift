//
//  YeniVC.swift
//  EFT
//
//  Created by Burak Topçu on 10.03.2018.
//  Copyright © 2018 Burak Topçu. All rights reserved.
//

import UIKit

class YeniVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate{
    
    private lazy var tittleView = UIView()
    private lazy var hesapView = UIView()
    private lazy var altTabView = UIView()
    private lazy var segmentedControl = UISegmentedControl()
    private lazy var hesapTxtView = UIView()
    private lazy var tutarLblView = UIView()
    private lazy var tutarTxtView = UIView()
    private lazy var tarihLblView = UIView()
    private lazy var tarihTxtView = UIView()
    private lazy var turLblView = UIView()
    private lazy var turTxtView = UIView()
    private lazy var infoMtnLblView = UIView()
    private lazy var aciklamaTxtView = UIView()
    private lazy var bttnsView = UIView()
    
    private lazy var hesapNoSemtLabel = UILabel()
    private lazy var bakiyeLabel = UILabel()
    private lazy var kBakiyeLabel = UILabel()
    private lazy var bakiyeBirimLabel = UILabel()
    private lazy var kBakiyeBirimLabel = UILabel()
    private lazy var arrowLabel = UILabel()
    
    private lazy var tittleViewLabel = UILabel()
    private lazy var altTabLabel = UILabel()
    private lazy var hesapTxtTextField = UITextField()
    private lazy var hesapTxtImg = UIImageView()
    private lazy var tutarLblLabel = UILabel()
    private lazy var tutarTxtTextField = UITextField()
    private lazy var tutarTxtTL = UILabel()
    private lazy var tarihLblLabel = UILabel()
    private lazy var tarihTxtTextField = UITextField()
    private lazy var tarihTxtImg = UIImageView()
    private lazy var turLblLabel = UILabel()
    private lazy var turTxtLabel = UILabel()
    private lazy var turTxtArrow = UILabel()
    private lazy var infoCheckLabel = UILabel()
    private lazy var infoTxtLabel = UILabel()
    private lazy var infoAciklamaLabel = UILabel()
    private lazy var aciklamaTxtTextView = UITextView()
    private lazy var aciklamaKalanLabel = UILabel()
    private lazy var aciklamaPenImg = UIImageView()
    private lazy var bttnDevamButton = UIButton()
    private lazy var bttnVazgecButton = UIButton()
    
    private var viewControllerHeight = CGFloat()
    private var collectedViews = UIView()
    private let tableView = UITableView()
    private var tableViewContent = UIView()
    private var sections = sectionsData
    private var selectedItemIndex = 0
    private var isCollapsed = true
    private var totalCollectsHeight: CGFloat = 0.0
    
    var hesapViewTopAnchor = NSLayoutConstraint()
    var tableViewContentHeightAnchor = NSLayoutConstraint()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        collectedViews.backgroundColor = UIColor.brown
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isUserInteractionEnabled = true
        tableViewContent.isUserInteractionEnabled = true
        self.view.addSubview(tittleView)
        self.view.addSubview(collectedViews)
        self.view.addSubview(hesapView)
        self.view.addSubview(tableViewContent)
        tableViewContent.addSubview(tableView)
        setSelectedItem()
        setCollects()
        setupLayout()
        setupAttiributes()
        setupSegmentedControl()
        setViewControllerHeight()
        let keyboardTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        keyboardTap.delegate = self
        self.view.addGestureRecognizer(keyboardTap)
        let hesapViewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.setHesapViewState))
        self.hesapView.addGestureRecognizer(hesapViewTap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        controllerCollaps()
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.tableView) == true {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GonderenHesapBilgisiTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? GonderenHesapBilgisiTableViewCell ??
            GonderenHesapBilgisiTableViewCell(style: .default, reuseIdentifier: "cell")
        
        let item: Item = sections[indexPath.section].items[indexPath.row]
        cell.hesapNoSemtLabel.text = "\(item.hesapNo) / \(item.semt)"
        cell.bakiyeLabel.text = "Bakiye"
        cell.kullanilabilirBakiyeLabel.text = "Kullanılabilir Bakiye"
        cell.bakiyeBirimLabel.text = "\(item.bakiye) TL"
        cell.kullanilabilirBakiyeBirimLabel.text = "\(item.kullanilabilirBakiye) TL"
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItemIndex = indexPath.row
        setSelectedItem()
        setHesapViewState()
    }
    
    private func setupSegmentedControl() {
        segmentedControl.removeAllSegments()
        segmentedControl.insertSegment(withTitle: "IBAN", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "Hesap", at: 1, animated: false)
        segmentedControl.insertSegment(withTitle: "Kredi Kartı", at: 2, animated: false)
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 1
    }
    @objc func selectionDidChange(_ sender: UISegmentedControl) {
    }
    
    private func controllerCollaps(){
        isCollapsed = false
        setHesapViewState()
    }
    
    private func setupLayout(){
        setTopTittleViewFrame()
        setHesapView()
        setCollectedViewFrame()
        setTableViewContent()
        setTableViewFrame()
    }

    private func setTopTittleViewFrame(){
        tittleView.translatesAutoresizingMaskIntoConstraints = false
        tittleView.heightAnchor.constraint(equalToConstant: 35.0).isActive = true
        tittleView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        tittleView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        tittleView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    }
    
    private func setTableViewFrame(){
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: tableViewContent.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: tableViewContent.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: tableViewContent.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: tableViewContent.bottomAnchor).isActive = true
    }
    private func setTableViewContent(){
        tableViewContent.translatesAutoresizingMaskIntoConstraints = false
        tableViewContentHeightAnchor = tableView.heightAnchor.constraint(equalToConstant: 0.0)
        tableViewContentHeightAnchor.isActive = true
        tableViewContent.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        tableViewContent.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        tableViewContent.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    private func setHesapView(){
        hesapView.translatesAutoresizingMaskIntoConstraints = false
        hesapViewTopAnchor = hesapView.topAnchor.constraint(equalTo: self.view.topAnchor)
        hesapViewTopAnchor.isActive = true
        hesapViewTopAnchor.constant = 35.0
        hesapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        hesapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        hesapView.heightAnchor.constraint(equalToConstant: 75.0).isActive = true
    }

    private func setCollects(){
        setViews(topView: collectedViews, currentView: altTabView, height: 75.0)
        setViews(topView: altTabView, currentView: hesapTxtView, height: 35.0)
        setViews(topView: hesapTxtView, currentView: tutarLblView, height: 30.0)
        setViews(topView: tutarLblView, currentView: tutarTxtView, height: 35.0)
        setViews(topView: tutarTxtView, currentView: tarihLblView, height: 30.0)
        setViews(topView: tarihLblView, currentView: tarihTxtView, height: 35.0)
        setViews(topView: tarihTxtView, currentView: turLblView, height: 30.0)
        setViews(topView: turLblView, currentView: turTxtView, height: 35.0)
        setViews(topView: turTxtView, currentView: infoMtnLblView, height: 75.0)
        setViews(topView: infoMtnLblView, currentView: aciklamaTxtView, height: 80.0)
        setViews(topView: aciklamaTxtView, currentView: bttnsView, height: 130.0)
    }
    private func setViews(topView: UIView, currentView: UIView, height: CGFloat){
        collectedViews.addSubview(currentView)
        currentView.translatesAutoresizingMaskIntoConstraints = false
        if currentView == altTabView{
            currentView.topAnchor.constraint(equalTo: topView.topAnchor).isActive = true
        }else{
            currentView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        }
        
        currentView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        currentView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        currentView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        currentView.heightAnchor.constraint(equalToConstant: height).isActive = true
        totalCollectsHeight += height
    }
    private func setCollectedViewFrame(){
        collectedViews.translatesAutoresizingMaskIntoConstraints = false
        collectedViews.heightAnchor.constraint(equalToConstant: totalCollectsHeight).isActive = true
        collectedViews.topAnchor.constraint(equalTo: hesapView.bottomAnchor).isActive = true
        collectedViews.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        collectedViews.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    }
    private func setupAttiributes(){
        let gray = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 100)
        let red = UIColor(red: 248/255, green: 28/255, blue: 53/255, alpha: 100)
        //top tittle
        tittleView.backgroundColor = gray
        tittleViewLabel.text = "Gönderen Hesap Bilgisi"
        tittleViewLabel.font = UIFont.systemFont(ofSize: 15.0)
        tittleViewLabel.textColor = UIColor.black
        tittleViewLabel.sizeToFit()
        tittleView.addSubview(tittleViewLabel)
        tittleViewLabel.translatesAutoresizingMaskIntoConstraints = false
        tittleViewLabel.centerYAnchor.constraint(equalTo: tittleView.centerYAnchor).isActive = true
        tittleViewLabel.leadingAnchor.constraint(equalTo: tittleView.leadingAnchor, constant: 14.0).isActive = true
        tittleViewLabel.heightAnchor.constraint(equalTo: tittleView.heightAnchor, multiplier: 0.7).isActive = true
        //alt tab
        altTabView.backgroundColor = gray
        altTabLabel.text = "Hesap No"
        altTabLabel.font = UIFont.systemFont(ofSize: 15.0)
        altTabLabel.textColor = UIColor.black
        altTabLabel.sizeToFit()
        altTabView.addSubview(altTabLabel)
        altTabLabel.translatesAutoresizingMaskIntoConstraints = false
        altTabLabel.bottomAnchor.constraint(equalTo: altTabView.bottomAnchor, constant: -8.0).isActive = true
        altTabLabel.leadingAnchor.constraint(equalTo: altTabView.leadingAnchor, constant: 14.0).isActive = true
        altTabLabel.heightAnchor.constraint(equalTo: tittleViewLabel.heightAnchor).isActive = true
        altTabView.addSubview(segmentedControl)
        segmentedControl.tintColor = red
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.topAnchor.constraint(equalTo: altTabView.topAnchor, constant: 8.0).isActive = true
        segmentedControl.leadingAnchor.constraint(equalTo: altTabView.leadingAnchor, constant: 14.0).isActive = true
        segmentedControl.trailingAnchor.constraint(equalTo: altTabView.trailingAnchor, constant: -14.0).isActive = true
        segmentedControl.heightAnchor.constraint(equalTo: altTabView.heightAnchor, multiplier: 0.35).isActive = true
        //hesap no
        hesapTxtView.backgroundColor = UIColor.white
        hesapTxtTextField.font = UIFont.boldSystemFont(ofSize: 15.0)
        hesapTxtTextField.text = "764951"
        hesapTxtTextField.isUserInteractionEnabled = true
        hesapTxtTextField.sizeToFit()
        hesapTxtView.addSubview(hesapTxtTextField)
        hesapTxtTextField.translatesAutoresizingMaskIntoConstraints = false
        hesapTxtTextField.centerYAnchor.constraint(equalTo: hesapTxtView.centerYAnchor).isActive = true
        hesapTxtTextField.leadingAnchor.constraint(equalTo: hesapTxtView.leadingAnchor, constant: 14.0).isActive = true
        hesapTxtTextField.heightAnchor.constraint(equalTo: hesapTxtView.heightAnchor, multiplier: 0.7).isActive = true
        hesapTxtView.addSubview(hesapTxtImg)
        hesapTxtImg.image = #imageLiteral(resourceName: "writingPen")
        hesapTxtImg.translatesAutoresizingMaskIntoConstraints = false
        hesapTxtImg.trailingAnchor.constraint(equalTo: hesapTxtView.trailingAnchor, constant: -15).isActive = true
        hesapTxtImg.centerYAnchor.constraint(equalTo: hesapTxtView.centerYAnchor).isActive = true
        hesapTxtImg.heightAnchor.constraint(equalTo: hesapTxtView.heightAnchor, multiplier: 0.6).isActive = true
        hesapTxtImg.widthAnchor.constraint(equalTo: hesapTxtImg.heightAnchor).isActive = true
        //tutar label
        tutarLblView.backgroundColor = gray
        tutarLblLabel.text = "Tutar"
        tutarLblLabel.font = UIFont.systemFont(ofSize: 15.0)
        tutarLblLabel.textColor = UIColor.black
        tutarLblLabel.sizeToFit()
        tutarLblView.addSubview(tutarLblLabel)
        tutarLblLabel.translatesAutoresizingMaskIntoConstraints = false
        tutarLblLabel.centerYAnchor.constraint(equalTo: tutarLblView.centerYAnchor).isActive = true
        tutarLblLabel.leadingAnchor.constraint(equalTo: tutarLblView.leadingAnchor, constant: 14.0).isActive = true
        tutarLblLabel.heightAnchor.constraint(equalTo: tutarLblView.heightAnchor, multiplier: 0.7).isActive = true
        //tutar text
        tutarTxtView.backgroundColor = UIColor.white
        tutarTxtTextField.font = UIFont.boldSystemFont(ofSize: 15.0)
        tutarTxtTextField.text = "1.000,00"
        tutarTxtTextField.isUserInteractionEnabled = true
        tutarTxtTextField.sizeToFit()
        tutarTxtView.addSubview(tutarTxtTextField)
        tutarTxtTextField.translatesAutoresizingMaskIntoConstraints = false
        tutarTxtTextField.centerYAnchor.constraint(equalTo: tutarTxtView.centerYAnchor).isActive = true
        tutarTxtTextField.leadingAnchor.constraint(equalTo: tutarTxtView.leadingAnchor, constant: 14.0).isActive = true
        tutarTxtTextField.heightAnchor.constraint(equalTo: tutarTxtView.heightAnchor, multiplier: 0.7).isActive = true
        tutarTxtView.addSubview(tutarTxtTL)
        tutarTxtTL.font = UIFont.boldSystemFont(ofSize: 15.0)
        tutarTxtTL.textAlignment = .right
        tutarTxtTL.text = "TL"
        tutarTxtTL.sizeToFit()
        tutarTxtTL.translatesAutoresizingMaskIntoConstraints = false
        tutarTxtTL.trailingAnchor.constraint(equalTo: tutarTxtView.trailingAnchor, constant: -12).isActive = true
        tutarTxtTL.centerYAnchor.constraint(equalTo: tutarTxtView.centerYAnchor).isActive = true
        tutarTxtTL.heightAnchor.constraint(equalTo: tutarTxtView.heightAnchor, multiplier: 0.7).isActive = true
        tutarTxtTL.widthAnchor.constraint(equalTo: tutarTxtTL.heightAnchor).isActive = true
        //tarih label
        tarihLblView.backgroundColor = gray
        tarihLblLabel.text = "İşlem Tarihi"
        tarihLblLabel.font = UIFont.systemFont(ofSize: 15.0)
        tarihLblLabel.textColor = UIColor.black
        tarihLblLabel.sizeToFit()
        tarihLblView.addSubview(tarihLblLabel)
        tarihLblLabel.translatesAutoresizingMaskIntoConstraints = false
        tarihLblLabel.centerYAnchor.constraint(equalTo: tarihLblView.centerYAnchor).isActive = true
        tarihLblLabel.leadingAnchor.constraint(equalTo: tarihLblView.leadingAnchor, constant: 14.0).isActive = true
        tarihLblLabel.heightAnchor.constraint(equalTo: tarihLblView.heightAnchor, multiplier: 0.7).isActive = true
        //tarih text
        tarihTxtView.backgroundColor = UIColor.white
        tarihTxtTextField.font = UIFont.boldSystemFont(ofSize: 15.0)
        tarihTxtTextField.text = "5/03/2018"
        tarihTxtTextField.isUserInteractionEnabled = true
        tarihTxtTextField.sizeToFit()
        tarihTxtView.addSubview(tarihTxtTextField)
        tarihTxtTextField.translatesAutoresizingMaskIntoConstraints = false
        tarihTxtTextField.centerYAnchor.constraint(equalTo: tarihTxtView.centerYAnchor).isActive = true
        tarihTxtTextField.leadingAnchor.constraint(equalTo: tarihTxtView.leadingAnchor, constant: 14.0).isActive = true
        tarihTxtTextField.heightAnchor.constraint(equalTo: tarihTxtView.heightAnchor, multiplier: 0.7).isActive = true
        tarihTxtView.addSubview(tarihTxtImg)
        tarihTxtImg.image = #imageLiteral(resourceName: "Calendar")
        tarihTxtImg.translatesAutoresizingMaskIntoConstraints = false
        tarihTxtImg.trailingAnchor.constraint(equalTo: tarihTxtView.trailingAnchor, constant: -15).isActive = true
        tarihTxtImg.centerYAnchor.constraint(equalTo: tarihTxtView.centerYAnchor).isActive = true
        tarihTxtImg.heightAnchor.constraint(equalTo: tarihTxtView.heightAnchor, multiplier: 0.6).isActive = true
        tarihTxtImg.widthAnchor.constraint(equalTo: tarihTxtImg.heightAnchor).isActive = true
        //tur label
        turLblView.backgroundColor = gray
        turLblLabel.text = "İşlem Türü"
        turLblLabel.font = UIFont.systemFont(ofSize: 15.0)
        turLblLabel.textColor = UIColor.black
        turLblLabel.sizeToFit()
        turLblView.addSubview(turLblLabel)
        turLblLabel.translatesAutoresizingMaskIntoConstraints = false
        turLblLabel.centerYAnchor.constraint(equalTo: turLblView.centerYAnchor).isActive = true
        turLblLabel.leadingAnchor.constraint(equalTo: turLblView.leadingAnchor, constant: 14.0).isActive = true
        turLblLabel.heightAnchor.constraint(equalTo: turLblView.heightAnchor, multiplier: 0.7).isActive = true
        // tür choose
        turTxtView.backgroundColor = UIColor.white
        turTxtLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
        turTxtLabel.text = "Diğer"
        tutarTxtTextField.sizeToFit()
        turTxtView.addSubview(turTxtLabel)
        turTxtLabel.translatesAutoresizingMaskIntoConstraints = false
        turTxtLabel.centerYAnchor.constraint(equalTo: turTxtView.centerYAnchor).isActive = true
        turTxtLabel.leadingAnchor.constraint(equalTo: turTxtView.leadingAnchor, constant: 14.0).isActive = true
        turTxtLabel.heightAnchor.constraint(equalTo: turTxtView.heightAnchor, multiplier: 0.7).isActive = true
        turTxtView.addSubview(turTxtArrow)
        turTxtArrow.font = UIFont.boldSystemFont(ofSize: 15.0)
        turTxtArrow.textAlignment = .right
        turTxtArrow.text = "〉"
        turTxtArrow.sizeToFit()
        turTxtArrow.translatesAutoresizingMaskIntoConstraints = false
        turTxtArrow.trailingAnchor.constraint(equalTo: turTxtView.trailingAnchor, constant: -12).isActive = true
        turTxtArrow.centerYAnchor.constraint(equalTo: turTxtView.centerYAnchor).isActive = true
        turTxtArrow.heightAnchor.constraint(equalTo: turTxtView.heightAnchor, multiplier: 0.7).isActive = true
        turTxtArrow.widthAnchor.constraint(equalTo: turTxtArrow.heightAnchor).isActive = true
        //info
        infoMtnLblView.backgroundColor = gray
        infoCheckLabel.text = " ✔ "
        infoCheckLabel.font = UIFont.systemFont(ofSize: 10.0)
        infoCheckLabel.backgroundColor = UIColor.white
        infoCheckLabel.layer.borderColor = UIColor.gray.cgColor
        infoCheckLabel.layer.borderWidth = 0.3
        infoCheckLabel.textColor = UIColor.black
        infoCheckLabel.sizeToFit()
        infoMtnLblView.addSubview(infoCheckLabel)
        infoCheckLabel.translatesAutoresizingMaskIntoConstraints = false
        infoCheckLabel.topAnchor.constraint(equalTo: infoMtnLblView.topAnchor, constant: 14.0).isActive = true
        infoCheckLabel.leadingAnchor.constraint(equalTo: infoMtnLblView.leadingAnchor, constant: 14.0).isActive = true
        infoCheckLabel.heightAnchor.constraint(equalTo: infoCheckLabel.widthAnchor).isActive = true
        let boldText = "IBAN bilgilendire metnini "
        let normalText = "okudum ve kabul ediyorum."
        let infoText2 = NSMutableAttributedString(string: normalText, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12)])
        let infoText1 = NSMutableAttributedString(string: boldText, attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 12)] )
        infoText1.append(infoText2)
        infoTxtLabel.attributedText = infoText1
        infoTxtLabel.textColor = UIColor.black
        infoTxtLabel.sizeToFit()
        infoMtnLblView.addSubview(infoTxtLabel)
        infoTxtLabel.translatesAutoresizingMaskIntoConstraints = false
        infoTxtLabel.centerYAnchor.constraint(equalTo: infoCheckLabel.centerYAnchor).isActive = true
        infoTxtLabel.leadingAnchor.constraint(equalTo: infoCheckLabel.trailingAnchor, constant: 12.0).isActive = true
        infoTxtLabel.heightAnchor.constraint(equalTo: infoCheckLabel.heightAnchor).isActive = true
        infoAciklamaLabel.font = UIFont.systemFont(ofSize: 15.0)
        infoAciklamaLabel.text = "Açıklama"
        infoAciklamaLabel.sizeToFit()
        infoMtnLblView.addSubview(infoAciklamaLabel)
        infoAciklamaLabel.translatesAutoresizingMaskIntoConstraints = false
        infoAciklamaLabel.bottomAnchor.constraint(equalTo: infoMtnLblView.bottomAnchor, constant: -10.0).isActive = true
        infoAciklamaLabel.leadingAnchor.constraint(equalTo: infoMtnLblView.leadingAnchor, constant: 14.0).isActive = true
        infoAciklamaLabel.heightAnchor.constraint(equalTo: altTabLabel.heightAnchor).isActive = true
        //açıklama
        aciklamaTxtView.backgroundColor = UIColor.white
        aciklamaTxtView.addSubview(aciklamaPenImg)
        aciklamaPenImg.image = #imageLiteral(resourceName: "writingPen")
        aciklamaPenImg.translatesAutoresizingMaskIntoConstraints = false
        aciklamaPenImg.topAnchor.constraint(equalTo: aciklamaTxtView.topAnchor, constant: 14.0).isActive = true
        aciklamaPenImg.trailingAnchor.constraint(equalTo: aciklamaTxtView.trailingAnchor, constant: -14.0).isActive = true
        aciklamaPenImg.heightAnchor.constraint(equalTo: hesapTxtImg.heightAnchor).isActive = true
        aciklamaPenImg.widthAnchor.constraint(equalTo: aciklamaPenImg.heightAnchor).isActive = true
        aciklamaKalanLabel.text = "Kalan: 21"
        aciklamaKalanLabel.sizeToFit()
        aciklamaKalanLabel.font = UIFont.systemFont(ofSize: 15)
        aciklamaKalanLabel.textColor = UIColor.red
        aciklamaKalanLabel.translatesAutoresizingMaskIntoConstraints = false
        aciklamaTxtView.addSubview(aciklamaKalanLabel)
        aciklamaKalanLabel.topAnchor.constraint(equalTo: aciklamaTxtView.topAnchor, constant: 14.0).isActive = true
        aciklamaKalanLabel.trailingAnchor.constraint(equalTo: aciklamaPenImg.leadingAnchor, constant: -13.0).isActive = true
        aciklamaTxtTextView.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        aciklamaTxtTextView.font = UIFont.systemFont(ofSize: 12.0)
        aciklamaTxtTextView.textColor = UIColor.black
        aciklamaTxtTextView.isScrollEnabled = false
        aciklamaTxtView.addSubview(aciklamaTxtTextView)
        aciklamaTxtTextView.translatesAutoresizingMaskIntoConstraints = false
        aciklamaTxtTextView.leadingAnchor.constraint(equalTo: aciklamaTxtView.leadingAnchor, constant: 14).isActive = true
        aciklamaTxtTextView.trailingAnchor.constraint(equalTo: aciklamaKalanLabel.leadingAnchor, constant: -13.0).isActive = true
        aciklamaTxtTextView.bottomAnchor.constraint(equalTo: aciklamaTxtView.bottomAnchor, constant: -14.0).isActive = true
        aciklamaTxtTextView.topAnchor.constraint(equalTo: aciklamaTxtView.topAnchor, constant: 14.0).isActive = true
        //buttons
        bttnsView.backgroundColor = gray
        bttnDevamButton.backgroundColor = red
        bttnDevamButton.setTitle("Devam", for: .normal)
        bttnDevamButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
        bttnDevamButton.setTitleColor(UIColor.white, for: .normal)
        bttnDevamButton.layer.cornerRadius = 10.0
        bttnDevamButton.translatesAutoresizingMaskIntoConstraints = false
        bttnsView.addSubview(bttnDevamButton)
        bttnDevamButton.bottomAnchor.constraint(equalTo: bttnsView.centerYAnchor, constant: -5.0).isActive = true
        bttnDevamButton.topAnchor.constraint(equalTo: bttnsView.topAnchor, constant: 14.0).isActive = true
        bttnDevamButton.leadingAnchor.constraint(equalTo: bttnsView.leadingAnchor, constant: 14.0).isActive = true
        bttnDevamButton.trailingAnchor.constraint(equalTo: bttnsView.trailingAnchor, constant: -14.0).isActive = true
        bttnVazgecButton.backgroundColor = UIColor.white
        bttnVazgecButton.setTitle("Vazgeç", for: .normal)
        bttnVazgecButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
        bttnVazgecButton.setTitleColor(UIColor.black, for: .normal)
        bttnVazgecButton.layer.cornerRadius = 10.0
        bttnVazgecButton.translatesAutoresizingMaskIntoConstraints = false
        bttnsView.addSubview(bttnVazgecButton)
        bttnVazgecButton.topAnchor.constraint(equalTo: bttnsView.centerYAnchor, constant: 5.0).isActive = true
        bttnVazgecButton.bottomAnchor.constraint(equalTo: bttnsView.bottomAnchor, constant: -14.0).isActive = true
        bttnVazgecButton.leadingAnchor.constraint(equalTo: bttnsView.leadingAnchor, constant: 14.0).isActive = true
        bttnVazgecButton.trailingAnchor.constraint(equalTo: bttnsView.trailingAnchor, constant: -14.0).isActive = true
        //current hesap view
        hesapView.backgroundColor = UIColor.white
        hesapView.addSubview(arrowLabel)
        arrowLabel.textColor = UIColor.black
        arrowLabel.font = UIFont.boldSystemFont(ofSize: 50.0)
        arrowLabel.text = "⌃"
        arrowLabel.sizeToFit()
        arrowLabel.translatesAutoresizingMaskIntoConstraints = false
//        arrowLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        arrowLabel.topAnchor.constraint(equalTo: hesapView.topAnchor, constant: 14.0).isActive = true
        arrowLabel.trailingAnchor.constraint(equalTo: hesapView.trailingAnchor, constant: -14.0).isActive = true
        arrowLabel.bottomAnchor.constraint(equalTo: hesapView.bottomAnchor, constant: -14.0).isActive = true
        hesapView.addSubview(hesapNoSemtLabel)
        hesapNoSemtLabel.textColor = UIColor.black
        hesapNoSemtLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
        hesapNoSemtLabel.sizeToFit()
        hesapNoSemtLabel.translatesAutoresizingMaskIntoConstraints = false
        hesapNoSemtLabel.topAnchor.constraint(equalTo: hesapView.topAnchor, constant: 8.0).isActive = true
        hesapNoSemtLabel.leadingAnchor.constraint(equalTo: hesapView.leadingAnchor, constant: 14.0).isActive = true
        hesapView.addSubview(kBakiyeLabel)
        kBakiyeLabel.textColor = UIColor.black
        kBakiyeLabel.font = UIFont.systemFont(ofSize: 13.0)
        kBakiyeLabel.text = "Kullanılabilir Bakiye"
        kBakiyeLabel.sizeToFit()
        kBakiyeLabel.translatesAutoresizingMaskIntoConstraints = false
        kBakiyeLabel.bottomAnchor.constraint(equalTo: hesapView.bottomAnchor, constant: -14.0).isActive = true
        kBakiyeLabel.leadingAnchor.constraint(equalTo: hesapView.leadingAnchor, constant: 14.0).isActive = true
        hesapView.addSubview(bakiyeLabel)
        bakiyeLabel.textColor = UIColor.black
        bakiyeLabel.font = UIFont.systemFont(ofSize: 13.0)
        bakiyeLabel.text = "Bakiye"
        bakiyeLabel.sizeToFit()
        bakiyeLabel.translatesAutoresizingMaskIntoConstraints = false
        bakiyeLabel.bottomAnchor.constraint(equalTo: kBakiyeLabel.topAnchor).isActive = true
        bakiyeLabel.leadingAnchor.constraint(equalTo: hesapView.leadingAnchor, constant: 14.0).isActive = true
        hesapView.addSubview(bakiyeBirimLabel)
        bakiyeBirimLabel.textColor = UIColor.black
        bakiyeBirimLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
        bakiyeBirimLabel.sizeToFit()
        bakiyeBirimLabel.translatesAutoresizingMaskIntoConstraints = false
        bakiyeBirimLabel.topAnchor.constraint(equalTo: bakiyeLabel.topAnchor).isActive = true
        bakiyeBirimLabel.bottomAnchor.constraint(equalTo: bakiyeLabel.bottomAnchor).isActive = true
        bakiyeBirimLabel.trailingAnchor.constraint(equalTo: arrowLabel.leadingAnchor, constant: -5.0).isActive = true
        hesapView.addSubview(kBakiyeBirimLabel)
        kBakiyeBirimLabel.textColor = UIColor.black
        kBakiyeBirimLabel.font = UIFont.boldSystemFont(ofSize: 13.0)
        kBakiyeBirimLabel.sizeToFit()
        kBakiyeBirimLabel.translatesAutoresizingMaskIntoConstraints = false
        kBakiyeBirimLabel.topAnchor.constraint(equalTo: kBakiyeLabel.topAnchor).isActive = true
        kBakiyeBirimLabel.bottomAnchor.constraint(equalTo: kBakiyeLabel.bottomAnchor).isActive = true
        kBakiyeBirimLabel.trailingAnchor.constraint(equalTo: arrowLabel.leadingAnchor, constant: -5.0).isActive = true
    }
    
    private func setViewControllerHeight(){
        viewControllerHeight = totalCollectsHeight + tittleView.frame.size.height + hesapView.frame.size.height
        self.view.frame.size.height = viewControllerHeight
    }
    
    private func setSelectedItem(){
     let item = sections[0].items[selectedItemIndex]
        let hesapNoSemt : String?
        let bakiye : String?
        let kBakiye : String?
        
        hesapNoSemt = "\(item.hesapNo) / \(item.semt)"
        bakiye = "\(item.bakiye) TL"
        kBakiye = "\(item.kullanilabilirBakiye) TL"
        
        guard let str1 = hesapNoSemt else {
            return
        }
        guard let str2 = bakiye else {
            return
        }
        guard let str3 = kBakiye else {
            return
        }
        
        hesapNoSemtLabel.text = str1
        bakiyeBirimLabel.text = str2
        kBakiyeBirimLabel.text = str3
    }
    @objc private func dismissKeyboard(){
        self.view.endEditing(true)
    }
    @objc private func setHesapViewState(){
        isCollapsed = !isCollapsed
        arrowLabel.rotate(isCollapsed ? 0.0 : .pi)
        collectedViews.isHidden = (isCollapsed ? false : true)
        tittleView.isHidden = (isCollapsed ? false : true)
        UIView.animate(withDuration: 0.2, animations: {
            self.hesapViewTopAnchor.constant = (self.isCollapsed ? 35.0 : 0.0)
            self.tableViewContentHeightAnchor.constant = (self.isCollapsed ? 0.0 : self.totalCollectsHeight)
            self.tableView.reloadData()
            self.view.layoutIfNeeded()
        })
        
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
//            self.hesapViewTopAnchor.constant = (self.isCollapsed ? 35.0 : 0.0)
//            self.tableViewContentHeightAnchor.constant = (self.isCollapsed ? 0.0 : self.totalCollectsHeight)
//            self.tableView.reloadData()
//            self.view.layoutIfNeeded()
//            }, completion: nil)
        
        
    }
}
