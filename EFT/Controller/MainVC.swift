//
//  MainVC.swift
//  EFT
//
//  Created by Burak Topçu on 9.03.2018.
//  Copyright © 2018 Burak Topçu. All rights reserved.
//

import UIKit

class MainVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var scroolView: UIScrollView!
    private var contentView = UIView()
    
    private lazy var yeniViewController : YeniVC = {
        let storyboard = UIStoryboard(name: "EFT", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "YeniVC") as! YeniVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    private lazy var kayitliViewController : KayitliVC = {
        let storyboard = UIStoryboard(name: "EFT", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "KayitliVC") as! KayitliVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    private lazy var sonYirmiViewController : SonYirmiVC = {
        let storyboard = UIStoryboard(name: "EFT", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "SonYirmiVC") as! SonYirmiVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroolView.delegate = self
        scroolView.addSubview(contentView)
        setupView()
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        contentView.addSubview(viewController.view)
        contentView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: viewController.view.frame.size.height + 30.0)
        scroolView.contentSize = CGSize(width: contentView.frame.size.width, height: contentView.frame.size.height)
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    
    private func setupView() {
        setupSegmentedControl()
        updateView()
    }
    private func setupSegmentedControl() {
        segmentedControl.removeAllSegments()
        segmentedControl.insertSegment(withTitle: "Yeni", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "Kayıtlı", at: 1, animated: false)
        segmentedControl.insertSegment(withTitle: "Son 20", at: 2, animated: false)
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 0
    }
    @objc func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }
    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: kayitliViewController)
            remove(asChildViewController: sonYirmiViewController)
            add(asChildViewController: yeniViewController)
            
        } else if segmentedControl.selectedSegmentIndex == 1 {
            remove(asChildViewController: yeniViewController)
            remove(asChildViewController: sonYirmiViewController)
            add(asChildViewController: kayitliViewController)
        }
        else if segmentedControl.selectedSegmentIndex == 2 {
            remove(asChildViewController: yeniViewController)
            remove(asChildViewController: kayitliViewController)
            add(asChildViewController: sonYirmiViewController)
        }
    }
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
}
 
